{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Python and Pandas \n",
    "\n",
    "\n",
    "\n",
    "## The importance of data preprocessing\n",
    "\n",
    "Data preprocessing (also called data wrangling, cleaning, scrubbing, etc) is the most important thing you will do with your data because it sets the stage for the analysis part of your data analysis workflow. The preprocessing you do largely depends on what kind of data you have, what sort of analysis you'll be doing with your data, and what you intend to do with the results.\n",
    "\n",
    "Preprocessing is also a process for getting to know your data, and can answer questions such as these (and more): \n",
    "\n",
    "- What kind of data are you working with? \n",
    "- Is it categorical, continuous, or a mix of both? \n",
    "- What's the distribution of features in your dataset? \n",
    "- What sort of wrangling do you have to do?\n",
    "- Do you have any missing data? \n",
    "- Do you need to remove missing data?\n",
    "- Do you need only a subset of your data?\n",
    "- Do you need more data?\n",
    "- Or less?\n",
    "\n",
    "The questions you'll have to answer are, again, dependent upon the data that you're working with, and preprocessing can be a way to figure that out.\n",
    "\n",
    "## What is Pandas?\n",
    "\n",
    "Pandas is by far my favorite preprocessing tool. It's a data wrangling/modeling/analysis tool that is similar to R and Excel; in fact, the DataFrame data structure in Pandas was named after the DataFrame in R. Pandas comes with several easy-to-use data structures, two of which (the `Series` and the `DataFrame`) I'll be covering here.\n",
    "\n",
    "I'll also be covering a bunch of different wrangling tools, as well as a couple of analysis tools.\n",
    "\n",
    "## Why Pandas?\n",
    "\n",
    "So, why would you want to use Python, as opposed to tools like R and Excel? I like to use it because I like to keep everything in Python, from start to finish. It just makes it easier if I don't have to switch back and forth between other tools. Also, if I have to build in preprocessing as part of a production system, which I've had to do at my job, it makes sense to just do it in Python from the beginning. \n",
    "\n",
    "Pandas is great for preprocessing, as we'll see, and it can be easily combined with other modules from the scientific Python stack.\n",
    "\n",
    "## Pandas data structures\n",
    "\n",
    "Pandas has several different data structures, but we're going to talk about the `Series` and the `DataFrame`.\n",
    "\n",
    "### The Series\n",
    "\n",
    "The `Series` is a one-dimensional array that can hold a variety of data types, including a mix of those types. The row labels in a `Series` are collectively called the index. You can create a `Series` in a few different ways. Here's how you'd create a `Series` from a list."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "import pandas as pd\n",
    "\n",
    "some_numbers = [2, 5, 7, 3, 8]\n",
    "\n",
    "series_1 = pd.Series(some_numbers)\n",
    "series_1"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To specify an index, you can also pass in a list."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "ind = ['a', 'b', 'c', 'd', 'e']\n",
    "\n",
    "series_2 = pd.Series(some_numbers, index=ind)\n",
    "series_2"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can pull that index back out again, too, with the `.index` attribute."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "series_2.index"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can also create a `Series` with a dictionary. The keys of the dictionary will be used as the index, and the values will be used as the `Series` array."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "more_numbers = {'a': 9, 'b': 'eight', 'c': 7.5, 'd': 6}\n",
    "\n",
    "series_3 = pd.Series(more_numbers)\n",
    "series_3"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Notice how, in that previous example, I created a `Series` with integers, a float, and a string."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### The DataFrame\n",
    "\n",
    "The `DataFrame` is Pandas' most used data structure. It's a two and greater dimensional structure that can also hold a variety of mixed data types. It's similar to a spreadsheet in Excel or a SQL table. You can create a `DataFrame` with a few different methods. First, let's look at how to create a `DataFrame` from multiple `Series` objects."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "combine_series = pd.DataFrame([series_2, series_3])\n",
    "combine_series"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Notice how in column `b`, we have two kinds of data. If a column in a `DataFrame` contains multiple types of data, the data type (or `dtype`) of the column will be chosen to accomodate all of the data. We can look at the data types of different columns with the `.dtypes` attribute. `object` is the most general, which is what has been chosen for column `b`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "combine_series.dtypes"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Another way to create a `DataFrame` is with a dictionary of lists. This is pretty straightforward:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "data = {'col1': ['i', 'love', 'pandas', 'so', 'much'],\n",
    "        'col2': ['so', 'will', 'you', 'i', 'promise']}\n",
    "\n",
    "df = pd.DataFrame(data)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## File I/O\n",
    "\n",
    "It's really easy to read data into Pandas from a file. Pandas will read your file directly into a `DataFrame`. There are multiple ways to read in files, but they all work in the same way. Here's how you read in a CSV file:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "pokemon_df = pd.read_csv('./dataset/Pokemon.csv')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "But how about the de facto dataset output of Web APIs? Can Pandas read the hallowed JSON file?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "pokejson= pd.read_json('./dataset/Pokemon.json')\n",
    "pokejson.head()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "##### Here's a quick description of each column in the DataFrame:\n",
    "\n",
    "* ```#``` - Pokedex entry number of the Pokemon\n",
    "* ```Name``` - name of the Pokemon\n",
    "* ```Type 1``` - each Pokemon has a type, this determines weakness/resistance to attacks [referred to as the primary type]\n",
    "* ```Type 2``` - some Pokemon are dual type and have 2 [referred to as the secondary type]\n",
    "* ```Total``` - sum of all stats that come after this, a general guide to how strong a Pokemon is\n",
    "* ```HP``` - hit points, or health, defines how much damage a Pokemon can withstand before fainting\n",
    "* ```Attack``` - the base modifier for normal attacks\n",
    "* ```Defense``` - the base damage resistance against normal attacks\n",
    "* ```Sp. Atk``` - special attack, the base modifier for special attacks\n",
    "* ```Sp. Def``` - the base damage resistance against special attacks\n",
    "* ```Speed``` - determines which Pokemon attacks first each round\n",
    "* ```Generation``` - refers to which grouping/game series the Pokemon was released in\n",
    "* ```Legendary``` - a boolean that identifies whether the Pokemon is legendary\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The `head` and `tail` function allows examining the last 5 rows in a dataframe. Moreover value `x` can be passed as parameter to both functions to specify the `x` number of rows to return from the dataframe"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "pokemon_df.head(3)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "pokemon_df.tail(3)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Another way of reading a file delimited by characters other than commas would be to specify the character in the `delimiter` parameter. Moreover, reading in a tab delimited file is just as easy. Make sure to pass in `'\\t'` to the delimiter parameter."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "my_pokemon_df = pd.read_csv('./dataset/Pokemon.csv', delimiter=',')\n",
    "my_pokemon_df.head(3)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Rename ```Type 1``` and ```Type 2``` columns to ```Primary``` and ```Secondary``` respectively"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "pokemon_df.columns = pokemon_df.columns.str.replace('Type 1', 'Primary')\n",
    "pokemon_df.columns = pokemon_df.columns.str.replace('Type 2', 'Secondary')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "pokemon_df.columns = pokemon_df.columns.str.replace('Sp. Atk', 'SpAtk')\n",
    "pokemon_df.columns = pokemon_df.columns.str.replace('Sp. Def', 'SpDef')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Exploring the data\n",
    "\n",
    "Here are some different ways to explore the data we have. Let's first take a look at some of the basic characteristics of the `pokemon_df` dataset. You can easily find the number of rows and the number of columns a dataframe has using the `.shape` attribute."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "pokemon_df.shape"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Getting column names from a `DataFrame` is also easy and can be done using the `.columns` attribute."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "pokemon_df.columns"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Another useful thing you can do is generate some summary statistics using the `describe()` function. The `describe()` function calculates descriptive statistics like the mean, standard deviation, and quartile values for continuous and integer data that exist in your dataset. Don't worry, Pandas won't try to calculate the standard deviation of your categorical values!"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "pokemon_df.describe()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Another useful thing you can do to explore your data is to sort it. Let's say we wanted to sort our `pokemon_df DataFrame` by `Speed`. This is very easy as well:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "pokemon_df.sort_values(by='Speed').head()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Working with dataframes\n",
    "\n",
    "Pandas has a ton of functionality for manipulating and wrangling the data. Let's look at a bunch of different ways to select and subset our data.\n",
    "\n",
    "### Selecting columns and rows\n",
    "\n",
    "There are multiple ways to select by both rows and columns. From index to slicing to label to position, there are a variety of methods to suit your data wrangling needs.\n",
    "\n",
    "Let's select just the `Defense` column from the `pokemon_df DataFrame`. This works similar to how you would access values from a dictionary:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "pokemon_df['Defense']"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can do exactly the same thing by using `Defense` as an attribute:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "pokemon_df.Defense"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To extract rows from a `DataFrame`, you can use the slice method, similar to how you would slice a list. Here's how we would grab rows 7-13 from the `pokemon_df DataFrame`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "pokemon_df[7:14]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Pandas also has tools for purely label-based selection of rows and columns using the `.loc` indexer. The `.loc` indexer takes input as `[row, column]`. \n",
    "\n",
    "For example, let's say we wanted to select the `Attack` value in the 8th instance in our `pokemon_df DataFrame`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "pokemon_df.loc[8,'Attack']"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can also use `.loc` to grab slices. It's important to note that `.loc` interprets the index as a *label*. This means that, if we select a range, it will grab the last item in the range, unlike slicing in a list. The index is the label for the rows. Let's grab the abv for rows 8 to 11 from the `pokemon_df DataFrame`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "pokemon_df.loc[8:11, 'Attack']"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Finally, let's just grab all columns for rows 8 to 11."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "pokemon_df.loc[8:11, :]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "So, `.loc` provides functionality for a very specific and precise selection method.\n",
    "\n",
    "Pandas has tools for purely position-based selection of rows and columns using the `.iloc` indexer, which works exactly how slicing a list works. The `.iloc` indexer also takes input as `[row, column]`, but takes only integer input. If we wanted to access the 9th row and the `Defense` value from `pokemon_df`, it would look like this (remember that integer indexing is 0-based):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "pokemon_df.iloc[9, 7]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To grab rows 60-63 and the `Total`,  `HP`,  `Attack`, and  `Defense` columns from the `pokemon_df DataFrame`, we would need to do the following:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "pokemon_df.iloc[60:64, 4:8]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "`.iloc` again works like slicing a list, based on position, so it does not grab the last item, like `.loc` does.\n",
    "\n",
    "To grab all values and the `Total`,  `HP`,  `Attack`, and  `Defense` columns from the `pokemon_df DataFrame`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "pokemon_df.iloc[:, 4:8]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "One of my favorite methods for selecting data is through boolean indexing. Boolean indexing is similar to the WHERE clause in SQL in that it allows you to filter out data based on certain criteria. Let's see how this works.\n",
    "\n",
    "Let's select from the `pokemon_df DataFrame` where `Legendary` is True."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "pokemon_df[pokemon_df['Legendary'] == True]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This works with any comparison operators, like >, < >=, !=, and so on. For example, we can select everything from the `pokemon_df DataFrame` where the value in the `Attack` column is less than 100."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "pokemon_df[pokemon_df['Attack'] < 50]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can also say 'not' with the tilde: ~\n",
    "\n",
    "Let's select from the `pokemon_df DataFrame` where `HP` is NOT greater than 100, which is equivalent to saying less than or equal to."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "pokemon_df[~(pokemon_df['HP'] > 100)]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Show the where the cutest pokemon is"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "electric_pokemons = pokemon_df[pokemon_df['Primary'] == 'Electric']\n",
    "electric_pokemons.head(10)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "It's also possible to combine these boolean indexers. Make sure you enclose them in parentheses. This is something I usually forget.\n",
    "\n",
    "Let's select from `pokemon_df` where `Attack` is less than 100 and `Type 1` is Grass."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "pokemon_df[(pokemon_df['Attack'] < 100) & (pokemon_df['Primary'] == \"Grass\")]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If you wanted to, you could just keep on chaining the booleans together. Let's add on where the `HP` is less than 50."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "pokemon_df[(pokemon_df['Attack'] < 100) & (pokemon_df['Primary'] == \"Grass\") & (pokemon_df['HP'] < 50)] "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Groupby\n",
    "\n",
    "`groupby()` is just like SQL's 'group by' clause. What ```groupby``` does is a three-step process:\n",
    "\n",
    "- Split the data\n",
    "- Apply a function to the split groups\n",
    "- Recombine the data\n",
    "\n",
    "In the apply step, you can do things like apply a statistical function, filter out data, or transform the data.\n",
    "\n",
    "Let's `groupby()` the `Type 1` in our  `pokemon_df` `DataFrame`! Let's start with just `groupby()`, and then build it from there. This will produce a `DataFrame groupby` object."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "pokemon_df.groupby('Primary')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Not so interesting yet. This object has some attributes you can access. We can get lists of which rows are in which group by using the `.groups` attribute:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "pokemon_df.groupby('Primary').groups"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The dataset was in order by `Type 1` to begin with, so that makes sense. To get just the keys, add the `.keys()` function to the end of that line."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "pokemon_df.groupby('Primary').groups.keys()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Expanding further the `pokemon_df` example, let's apply an aggregate function. Let's generate the mean of all the other values and group them by `Type 1`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "pokemon_df.groupby('Primary').mean()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "It's also possible to apply multiple functions to the entire `DataFrame` using the `agg()` function. Let's get not only the mean, but the count and the standard deviation as well for each value in the `DataFrame`, still grouping by `Type 1`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "pokemon_df.groupby('Primary').agg(['mean', 'count', 'std'])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Pivoting\n",
    "\n",
    "You can pivot in Pandas just like you would in Excel. `pivot_table()` takes in four requires parameters: the `DataFrame`, the column to use for the index, the column to use for the columns, and the column to use for the values. `pivot_table()` also has an `aggfunc` parameter that defaults to the mean of the values, but you can pass in other functions, just as we did in the `agg()` function before.\n",
    "\n",
    "#### Let's look at the mean `Attack` per `Type 1` and `Generation` combination."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "pd.pivot_table(pokemon_df, values='Attack', index='Primary', columns='Generation')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Alternative library for munging and transforming DataFrames\n",
    "I often use R’s dplyr package for exploratory data analysis and data manipulation. In addition to providing a consistent set of functions that one can use to solve the most common data manipulation problems, dplyr also allows one to write elegant, chainable data manipulation code using pipes.\n",
    "\n",
    "https://github.com/kieferk/dfply \n",
    "\n",
    "dplyr-style piping operations for pandas dataframes \n",
    "\n",
    "#### To install type: \n",
    "```pip install dfply```\n",
    "\n",
    "\n",
    "Importing the dfply library:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from dfply import *"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "pokemon_df >> head(3)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "pokemon_df >> tail(3)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can chain piped operations, and of course assign the output to a new DataFrame."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "my_pokemon = pokemon_df >> head(20) >> tail(5)\n",
    "my_pokemon"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### The X DataFrame symbol\n",
    "\n",
    "The DataFrame as it is passed through the piping operations is represented by the symbol X."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "pokemon_df >> select(X.Name, X.Primary) >> head(3)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Selecting and dropping\n",
    "select() and drop() functions\n",
    "\n",
    "There are two functions for selection, inverse of each other: select and drop. The select and drop functions accept string labels, integer positions, and/or symbolically represented column names (X.column)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "pokemon_df >> select(X.Name, X.Primary, X.Total, X.Legendary) >> head(3)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If you were instead to use drop, you would get back all columns besides those specified."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "pokemon_df >> drop(X.Total, X.Secondary, X.Legendary) >> head(3)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Subsetting and filtering\n",
    "### row_slice()\n",
    "\n",
    "Slices of rows can be selected with the row_slice() function. You can pass single integer indices or a list of indices to select rows as with. This is going to be the same as using pandas' .iloc."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "pokemon_df >> row_slice([5,15])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### sample()\n",
    "\n",
    "The sample() function functions exactly the same as pandas' .sample() method for DataFrames. Arguments and keyword arguments will be passed through to the DataFrame sample method."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "pokemon_df >> sample(n=5, replace=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### distinct()\n",
    "\n",
    "Selection of unique rows is done with distinct(), which similarly passes arguments and keyword arguments through to the DataFrame's .drop_duplicates() method."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "pokemon_df >> distinct(X.Name) >> tail(3)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### DataFrame transformation\n",
    "### mutate()\n",
    "\n",
    "New variables can be created with the mutate() function "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "pokemon_deltas = pokemon_df >> mutate(DeltaAtk= X.SpAtk- X.Attack, DeltaDef= X.SpDef- X.Defense)\n",
    "pokemon_deltas >> head(5)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Grouping\n",
    "### group_by() and ungroup()\n",
    "\n",
    "DataFrames are grouped along variables using the group_by() function and ungrouped with the ungroup() function. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "pokemon_deltas >> group_by(X.Legendary) >> tail()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Reshaping\n",
    "### arrange()\n",
    "\n",
    "Sorting is done by the arrange() function, which wraps around the pandas .sort_values() DataFrame method"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "pokemon_deltas >> arrange(X.DeltaAtk, ascending=False) >> head(5)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "pokemon_deltas >> arrange(X.DeltaDef, ascending=False) >> head(5)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### What are the 'tank' species in Pokemon?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "pokemon_tanks = pokemon_deltas >> arrange(X.HP, ascending= False)\n",
    "pokemon_tanks >> head(5)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### What are the DPS species in Pokemon?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "pokemon_dps = pokemon_deltas >> mutate(TotalAttack= X.Attack + X.SpAtk)\n",
    "pokemon_dps >> head()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "pokemon_dps >> arrange(X.TotalAttack, ascending=False) >> head(5)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Data Visualization with plotnine \n",
    "http://plotnine.readthedocs.io/en/stable/\n",
    "\n",
    "```plotnine``` is an implementation of a grammar of graphics in Python, it is based on ```ggplot2```. The grammar allows users to compose plots by explicitly mapping data to the visual objects that make up the plot.\n",
    "\n",
    "Plotting with a grammar is powerful, it makes custom (and otherwise complex) plots are easy to think about and then create, while the simple plots remain simple."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Installation\n",
    "Dependencies\n",
    "* ```matplotlib``` - Plotting backend.\n",
    "* ```pandas``` - Data handling.\n",
    "* ```mizani``` - Scales framework.\n",
    "* ```statsmodels``` - For various statistical computations.\n",
    "* ```scipy``` - For various statistical computation procedures.\n",
    "\n",
    "On the terminal type: ```pip install plotnine```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "##### Loading the ```plotnine``` library"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from plotnine import *\n",
    "from plotnine.data import *\n",
    "\n",
    "import warnings\n",
    "warnings.filterwarnings('ignore')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### How it works\n",
    "Making plots is a very repetetive: draw this line, add these colored points, then add these, etc. Instead of re-using the same code over and over, ggplot implements them using a high-level but very expressive API. The result is less time spent creating your charts, and more time interpreting what they mean.\n",
    "\n",
    "### Data source for plotnine\n",
    "```plotnine``` has a symbiotic relationship with ```pandas```. If you're planning on using plotnine, it's best to keep your data in DataFrames"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Back to Pokemon Land\n",
    "For the visualization phase of our data analysis, we aim to answer the following questions:\n",
    "* How are Pokemon numbers distributed across generations?\n",
    "* What are the most common types of Pokemon?\n",
    "* What are the Tank and DPS Pokemon species?\n",
    "* What are the fastest and slowest Pokemon species?\n",
    "\n",
    "#### Our exploratory visualization begins with the usual historam to determine the number of pokemons per ```Type``` and ```Generation```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ggplot(pokemon_dps, aes(x='Generation')) + geom_bar()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### A bar plot for summarizing the number of Pokemon species per ```Generation```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ggplot(pokemon_dps, aes(x='Primary')) + geom_bar()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ggplot(pokemon_dps, aes(x='Generation')) + geom_bar()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Using the ```pokemon_df``` dataframe we can create a scatterplot to explore the relationship between ```Attack``` and ```Defense```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ggplot(pokemon_dps, aes(x='Attack', y='Defense')) + geom_point()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Furthermore we can choose to add an ``alpha`` parameter for transparency adjustments for the scatterplot of ```Attack``` vs ```Defense```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ggplot(pokemon_dps, aes(x='Attack', y='Defense')) + geom_point(alpha= 0.6)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### For clarity between the different types of pokemons we use the ```Type1``` column as legend for the scatterplot"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ggplot(pokemon_dps, aes(x='Attack', y='Defense', color='Primary')) + geom_point()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### What is the ```Attack``` vs ```Defense``` among Legendary and non-Legendary Pokemon species?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ggplot(pokemon_dps, aes(x='Attack', y='Defense', color='Legendary')) + geom_point()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Separating the Legendary and the non-Legendary species by facetting and using ```Primary``` as legend"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ggplot(pokemon_dps, aes(x='Attack', y='Defense' , color='Primary')) + geom_point()+ facet_wrap('Legendary')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Separating the Legendary and the non-Legendary species via ```Speed``` vs ```HP``` by facetting and using ```Legendary``` as legend"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ggplot(pokemon_dps, aes(x='Speed', y='HP' , color = 'factor(Legendary)')) + geom_point(alpha= 0.6) + facet_wrap('Primary')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### ```Attack``` vs ```Defense``` through ```Generation```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ggplot(pokemon_dps, aes(x='Attack', y='Defense' , color = 'Primary')) + geom_point() + facet_wrap('Generation')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ggplot(pokemon_dps, aes(x='Attack', y='SpAtk' , color = 'DeltaAtk')) + geom_point() + facet_wrap('Generation')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### ```Speed``` vs ```HP``` through ```Generation```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ggplot(pokemon_dps, aes(x='Speed', y='HP' , color = 'TotalAttack')) + geom_point() + facet_wrap('Generation')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ggplot(pokemon_dps, aes(x='Attack', y='Defense', color='TotalAttack')) + geom_point() + facet_wrap('Primary')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### The ```plotnine``` API allows for theming of rendered plots"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ggplot(pokemon_dps, aes(x='Speed', y='HP' , color = 'TotalAttack')) + geom_point() + facet_wrap('Primary') + theme_538()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ggplot(pokemon_dps, aes(x='Speed', y='HP' , color = 'TotalAttack')) + geom_point() + facet_wrap('Primary') + theme_bw()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ggplot(pokemon_dps, aes(x='Speed', y='HP' , color = 'TotalAttack')) + geom_point() + facet_wrap('Primary') + theme_light()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ggplot(pokemon_dps, aes(x='Speed', y='HP' , color = 'TotalAttack')) + geom_point() + facet_wrap('Primary') + theme_minimal()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ggplot(pokemon_dps, aes(x='Speed', y='HP' , color = 'TotalAttack')) + geom_point(alpha= 0.7) + facet_wrap('Generation') + theme_seaborn()"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
