# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

This contains the Jupyter Notebook, Pokemon datasets used in the 9th PyTsada Meetup talk last 11 March 2017 and the 1st Python Roadcamp at Mindanao State University at Naawan, Misamis Oriental, PH

### How do I get set up? ###

* Install conda for your platform https://conda.io/miniconda.html
* navigate to the pokemon-pandas directory
* in the CLI type : ```conda create --name datascience pip python=3.6```
* to activate the virtual environment type: ```source activate datascience```
* Install packages and dependencies: ```pip install -r requirements.txt```
* run the JupyterLab notebook:  ```jupyter lab```
* open the notebook in the browser explorer 

### Acknowledgement ###

The text portions of the notebook are taken from Sarah Guido's material delivered from 
Hands-on Data Analysis with Python - PyCon 2015. Merci. :)

### Who do I talk to? ###

* Repo owner or admin https://www.facebook.com/aryan.limjap
* https://www.facebook.com/groups/itgpytsada/